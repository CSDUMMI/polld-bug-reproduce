const IPFSHttp = require("ipfs-http-client")

async function main() {
  const ipfs = IPFSHttp.create()
  const res = await ipfs.dag.put({
    a: 1,
    b: 2,
    c: 3,
  }, {
    format: 'dag-cbor',
    hashAlg: 'sha2-512'
  })
}

main()